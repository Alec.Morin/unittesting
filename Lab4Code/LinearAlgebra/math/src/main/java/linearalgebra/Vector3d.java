//Alec Morin    2239209
package linearalgebra;

public class Vector3d {
    
    private double x;
    private double y;
    private double z;

    public Vector3d(double newX, double newY, double newZ){
        this.x = newX;
        this.y = newY;
        this.z = newZ;
    }

    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    public double dotProduct(Vector3d secondVect){
        return (this.x * secondVect.getX()) + (this.y * secondVect.getY()) + (this.z * secondVect.getZ());
    }

    public Vector3d add(Vector3d secondVect){
        double a = this.x + secondVect.getX();
        double b = this.y + secondVect.getY();
        double c = this.z + secondVect.getZ();
        Vector3d newVect = new Vector3d(a, b, c);
        return newVect;
    }
}
